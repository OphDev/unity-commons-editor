# Unity Commons - Editor

This repository contains a collection of Editor scripts for Unity I built over time. I use them for my personal projects and change them as I need, sometimes breaking compatibility. Therefore, I offer no support for them but you can see them as a collection of useful gists.

Still beware of older scripts: as long as they compile without warning, I don't update them, but some may not be relevant anymore with the newest versions of Unity.

The StagPoint and UnityToolbag folders contain partial copies of unmaintained projects, and contain their own licenses.

## Dependencies

None
